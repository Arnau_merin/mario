{
    "id": "045bade3-0eaa-4a67-952d-57b8061923d0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_enemy21",
    "eventList": [
        {
            "id": "7a60d4e1-bfef-4807-846e-be625dc8c0a4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "045bade3-0eaa-4a67-952d-57b8061923d0"
        },
        {
            "id": "5e08a5d9-8098-4465-987a-a4d5827b54e1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "045bade3-0eaa-4a67-952d-57b8061923d0"
        },
        {
            "id": "e86de3b3-9c05-46be-8ce1-d91fc4e1e7d2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "045bade3-0eaa-4a67-952d-57b8061923d0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "2da110b0-51ff-439c-b206-37a8edfcede7",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "dbd6933e-3c32-48ab-949d-fef16d705f08",
    "visible": true
}