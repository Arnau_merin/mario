{
    "id": "2da110b0-51ff-439c-b206-37a8edfcede7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_enemy1",
    "eventList": [
        {
            "id": "a940a6e5-e552-4f14-840e-8cb4ccc7e7e7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2da110b0-51ff-439c-b206-37a8edfcede7"
        },
        {
            "id": "6070ede6-7c73-4e9c-a0a1-88ac40ffd4fe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2da110b0-51ff-439c-b206-37a8edfcede7"
        },
        {
            "id": "adb4b9ce-1ffc-43a1-8210-b0521c5c6a5b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "2da110b0-51ff-439c-b206-37a8edfcede7"
        },
        {
            "id": "05de7a31-0775-44dc-8d6e-74bb4f8f050d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "b32de984-76f7-4280-a689-b0b9b565991a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2da110b0-51ff-439c-b206-37a8edfcede7"
        },
        {
            "id": "92cc566d-440d-4845-80c6-f9087dda1719",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "36f38143-5618-4504-aef5-0de5d0133762",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2da110b0-51ff-439c-b206-37a8edfcede7"
        },
        {
            "id": "a9081e76-6d54-41ae-939f-14a4d3a2367a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4860f042-971e-4245-8cd1-683bd9d97547",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "2da110b0-51ff-439c-b206-37a8edfcede7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0f40999b-c806-4798-809b-7149ab6799c2",
    "visible": true
}