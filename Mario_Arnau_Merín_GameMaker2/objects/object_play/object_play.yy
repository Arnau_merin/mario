{
    "id": "2558f8ae-315d-4b50-a07e-48d7c0f7a625",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_play",
    "eventList": [
        {
            "id": "98c984a7-463c-4ba1-8853-400161bc1ea8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2558f8ae-315d-4b50-a07e-48d7c0f7a625"
        },
        {
            "id": "fb0e6d40-2324-496d-8a80-2f026e4c66d8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "2558f8ae-315d-4b50-a07e-48d7c0f7a625"
        },
        {
            "id": "ce176856-0b0f-46e8-acdc-135890ab8aca",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "2558f8ae-315d-4b50-a07e-48d7c0f7a625"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c28087b1-5e40-4e10-b13f-6ab3dc5ba334",
    "visible": true
}