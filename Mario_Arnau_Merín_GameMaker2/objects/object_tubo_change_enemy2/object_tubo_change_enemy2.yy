{
    "id": "36f38143-5618-4504-aef5-0de5d0133762",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_tubo_change_enemy2",
    "eventList": [
        {
            "id": "fd4d83b4-5a78-45a1-866f-6bbb23325394",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "36f38143-5618-4504-aef5-0de5d0133762"
        },
        {
            "id": "6afdfaee-5ec2-474f-ac3d-8e415cf99a90",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "36f38143-5618-4504-aef5-0de5d0133762"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e882d06c-c3cb-473e-92d3-b02d4e5ff6fc",
    "visible": true
}