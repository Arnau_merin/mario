{
    "id": "f9f3f38a-619e-4eef-b31c-26e19e11c8b5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_coin1",
    "eventList": [
        {
            "id": "1da80552-8bfb-4b40-9162-5ea533ee5da9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "d530d955-7ce6-4bbe-b1fa-62b1c877d097",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "f9f3f38a-619e-4eef-b31c-26e19e11c8b5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4a0e589f-002a-4721-be13-9def464a28ca",
    "visible": true
}