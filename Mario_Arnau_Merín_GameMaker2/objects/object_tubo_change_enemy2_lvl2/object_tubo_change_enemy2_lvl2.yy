{
    "id": "a0008fd7-1011-4b48-ab18-a86f21b5fcc4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_tubo_change_enemy2_lvl2",
    "eventList": [
        {
            "id": "25098d5b-0151-42a7-ab03-0d2d9e93d610",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a0008fd7-1011-4b48-ab18-a86f21b5fcc4"
        },
        {
            "id": "6185b3d6-ba9c-4a5a-a820-01f1f1c273a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a0008fd7-1011-4b48-ab18-a86f21b5fcc4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e882d06c-c3cb-473e-92d3-b02d4e5ff6fc",
    "visible": true
}