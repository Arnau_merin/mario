{
    "id": "5b76b6cf-bf4b-497d-96d5-3a2075b62cc0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_enemy",
    "eventList": [
        {
            "id": "bd07bc3e-5432-4143-8efc-f14fb5da24b8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5b76b6cf-bf4b-497d-96d5-3a2075b62cc0"
        },
        {
            "id": "8551eb22-5abb-45cd-97bf-e325030758fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "5b76b6cf-bf4b-497d-96d5-3a2075b62cc0"
        },
        {
            "id": "bb9fb65f-b838-439b-b2c8-d58d2a5e4b64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "5b76b6cf-bf4b-497d-96d5-3a2075b62cc0"
        },
        {
            "id": "f48ec51e-97d4-442f-85c7-6bccda7a37fd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "784b087e-d688-4f45-8cbf-fa41747a1f0c",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5b76b6cf-bf4b-497d-96d5-3a2075b62cc0"
        },
        {
            "id": "7b8a076c-e7b9-43c6-93cc-5a162345541c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "36f38143-5618-4504-aef5-0de5d0133762",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5b76b6cf-bf4b-497d-96d5-3a2075b62cc0"
        },
        {
            "id": "fd9ed7f7-6e5e-42a9-996c-f93381277a82",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "4860f042-971e-4245-8cd1-683bd9d97547",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "5b76b6cf-bf4b-497d-96d5-3a2075b62cc0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ce0e938f-93d3-4206-b865-987a0269d9bc",
    "visible": true
}