{
    "id": "68a32f67-c66e-4af3-8149-681f4c117102",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_tubo_enemy2_lvl2",
    "eventList": [
        {
            "id": "2b5140a9-e729-4d55-ac75-b8b28951a17a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "68a32f67-c66e-4af3-8149-681f4c117102"
        },
        {
            "id": "ff7da455-d401-4b42-b581-16b9bcebf686",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "68a32f67-c66e-4af3-8149-681f4c117102"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "27ae2eb5-8b24-4507-a2fe-fbc8eb5b5363",
    "visible": true
}