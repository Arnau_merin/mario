{
    "id": "d530d955-7ce6-4bbe-b1fa-62b1c877d097",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player1",
    "eventList": [
        {
            "id": "e0b37daf-ccfd-476d-acc2-f36a788c1bf6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d530d955-7ce6-4bbe-b1fa-62b1c877d097"
        },
        {
            "id": "b036d4dc-2967-42f3-89a1-4645cb19459f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d530d955-7ce6-4bbe-b1fa-62b1c877d097"
        },
        {
            "id": "67260170-a370-4e20-9132-7a5a86b0912f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "d530d955-7ce6-4bbe-b1fa-62b1c877d097"
        },
        {
            "id": "63d73304-ecce-478a-bd29-7f32cb8253db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "d530d955-7ce6-4bbe-b1fa-62b1c877d097"
        },
        {
            "id": "39c37e5c-d7cc-4b62-90e5-27938ee1d89c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "d530d955-7ce6-4bbe-b1fa-62b1c877d097"
        },
        {
            "id": "23784b2a-a09b-435c-a34c-cc7d11736a84",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "2da110b0-51ff-439c-b206-37a8edfcede7",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d530d955-7ce6-4bbe-b1fa-62b1c877d097"
        },
        {
            "id": "865e031c-ae4c-4f75-8891-cea9344d6186",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "045bade3-0eaa-4a67-952d-57b8061923d0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "d530d955-7ce6-4bbe-b1fa-62b1c877d097"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cf5deb1c-5485-41b9-a29b-ef7f9ce48349",
    "visible": true
}