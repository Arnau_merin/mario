{
    "id": "d37f205e-5d82-4e18-b74a-735e3c2d8cbb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_tubo_enemy2",
    "eventList": [
        {
            "id": "eca38f2d-6707-427d-a795-30aad091288e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d37f205e-5d82-4e18-b74a-735e3c2d8cbb"
        },
        {
            "id": "51b30e61-960a-4fb8-b563-645b4e40e591",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d37f205e-5d82-4e18-b74a-735e3c2d8cbb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "27ae2eb5-8b24-4507-a2fe-fbc8eb5b5363",
    "visible": true
}