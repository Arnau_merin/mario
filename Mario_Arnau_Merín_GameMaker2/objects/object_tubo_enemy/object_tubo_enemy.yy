{
    "id": "ced75d95-c90a-42a0-8c5e-0ae15a1a8c47",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_tubo_enemy",
    "eventList": [
        {
            "id": "577ac5fc-a524-45f7-bb93-68508b624847",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ced75d95-c90a-42a0-8c5e-0ae15a1a8c47"
        },
        {
            "id": "952bb36f-e8de-4222-a08d-de2df40e2ea7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ced75d95-c90a-42a0-8c5e-0ae15a1a8c47"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a5fa9d6-29f0-48cf-aa2c-9a01f4d84d08",
    "visible": true
}