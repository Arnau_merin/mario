{
    "id": "49e5978e-d753-4e6d-8c89-966b144098a0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_tubo_enemy_lvl2",
    "eventList": [
        {
            "id": "b7386711-34e2-4ae7-916e-73d7149e254d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "49e5978e-d753-4e6d-8c89-966b144098a0"
        },
        {
            "id": "ce524438-12af-4d30-b106-c730dbc9e499",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "49e5978e-d753-4e6d-8c89-966b144098a0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2a5fa9d6-29f0-48cf-aa2c-9a01f4d84d08",
    "visible": true
}