{
    "id": "f3bb3e0f-1fd1-497e-8da8-5d8e651fec76",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_high",
    "eventList": [
        {
            "id": "675f4a3c-15a5-436b-985b-9c6eee147084",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f3bb3e0f-1fd1-497e-8da8-5d8e651fec76"
        },
        {
            "id": "544fff74-47f5-4cad-9056-a1f2f855e444",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f3bb3e0f-1fd1-497e-8da8-5d8e651fec76"
        },
        {
            "id": "0dd13229-fa95-4590-acdb-1f4afcba6dd7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "f3bb3e0f-1fd1-497e-8da8-5d8e651fec76"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7e5091d2-a147-4737-a39a-e6a9b5c4e73c",
    "visible": true
}