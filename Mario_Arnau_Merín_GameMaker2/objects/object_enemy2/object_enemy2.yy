{
    "id": "fea96222-561b-44c6-9de6-d43e373bf086",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_enemy2",
    "eventList": [
        {
            "id": "c204580a-b690-4e52-8dfb-c3e01cedca15",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fea96222-561b-44c6-9de6-d43e373bf086"
        },
        {
            "id": "5ab26ab3-2bf2-414e-a149-53cd9b91e7a7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "fea96222-561b-44c6-9de6-d43e373bf086"
        },
        {
            "id": "d202259f-c05d-4648-abc8-4a12b797d408",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "fea96222-561b-44c6-9de6-d43e373bf086"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "5b76b6cf-bf4b-497d-96d5-3a2075b62cc0",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "82ee3de4-62ea-4e62-80bc-03314edbd03a",
    "visible": true
}