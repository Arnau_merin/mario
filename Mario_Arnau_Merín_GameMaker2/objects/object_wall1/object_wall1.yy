{
    "id": "b32de984-76f7-4280-a689-b0b9b565991a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall1",
    "eventList": [
        {
            "id": "35dbed8e-7060-4aaa-81f6-4f788e1902e5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b32de984-76f7-4280-a689-b0b9b565991a"
        },
        {
            "id": "46fdfa61-7b98-412e-9c67-f8c2b28be7f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "b32de984-76f7-4280-a689-b0b9b565991a"
        },
        {
            "id": "6d086ce0-0ec8-4421-b15e-bbfc167480e0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b32de984-76f7-4280-a689-b0b9b565991a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a7237d55-44e2-4052-aad1-f80d947e1518",
    "visible": true
}