{
    "id": "9b574576-513a-460b-8aa0-9c6b7fc7d238",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_coin",
    "eventList": [
        {
            "id": "a269d026-0728-4272-998d-3f9ef984ed1c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "9b574576-513a-460b-8aa0-9c6b7fc7d238"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4a0e589f-002a-4721-be13-9def464a28ca",
    "visible": true
}