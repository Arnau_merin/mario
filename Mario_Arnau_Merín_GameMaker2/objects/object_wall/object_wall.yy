{
    "id": "784b087e-d688-4f45-8cbf-fa41747a1f0c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_wall",
    "eventList": [
        {
            "id": "ce8a283d-dc6f-4b54-8921-9d92f4e05dce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "784b087e-d688-4f45-8cbf-fa41747a1f0c"
        },
        {
            "id": "68cc89e4-f734-43f9-862b-853259974357",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "784b087e-d688-4f45-8cbf-fa41747a1f0c"
        },
        {
            "id": "7ee5f079-120e-4095-a732-dc99ce0c1c36",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "784b087e-d688-4f45-8cbf-fa41747a1f0c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7a877a99-3f48-46a2-9293-eba0bbbdf445",
    "visible": true
}