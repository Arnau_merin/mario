{
    "id": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_player",
    "eventList": [
        {
            "id": "45ed937b-f1b0-4ede-a4a9-a626bec38ecb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66"
        },
        {
            "id": "3f2ad03e-8180-407e-8ab0-918122f99513",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66"
        },
        {
            "id": "4543922b-6d18-4ba8-853b-e8b558beb73f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66"
        },
        {
            "id": "472b4eb7-8fa5-4eae-8d75-caa6c36fb964",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66"
        },
        {
            "id": "5fc806ee-41bf-42b8-a134-986b419b0c01",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66"
        },
        {
            "id": "bf7689c2-57a0-4382-8991-3a1b87232102",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "5b76b6cf-bf4b-497d-96d5-3a2075b62cc0",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66"
        },
        {
            "id": "a0ec6643-49b6-47c3-aca7-1c1d9493ddd2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "fea96222-561b-44c6-9de6-d43e373bf086",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66"
        },
        {
            "id": "9d0c1245-8e4a-45f2-b06b-bf8c05b1d668",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66"
        },
        {
            "id": "b118422b-1def-41b2-beff-9b01a4464d8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "cf5deb1c-5485-41b9-a29b-ef7f9ce48349",
    "visible": true
}