{
    "id": "bb597786-3d31-4c50-82b7-85fbce45fecb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_enemy_test1",
    "eventList": [
        {
            "id": "ccdd6742-9456-4aeb-9d0a-03247920a73b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bb597786-3d31-4c50-82b7-85fbce45fecb"
        },
        {
            "id": "16cb8839-975c-42d8-aac8-704169a83d2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bb597786-3d31-4c50-82b7-85fbce45fecb"
        },
        {
            "id": "9899a347-62d6-479b-a80c-f156b0688c50",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 7,
            "m_owner": "bb597786-3d31-4c50-82b7-85fbce45fecb"
        },
        {
            "id": "8f84f851-1b2c-4b8e-8502-f6121bb0341d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "7a00e392-b32a-4b9c-b12f-3c57fa2a8f66",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bb597786-3d31-4c50-82b7-85fbce45fecb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}