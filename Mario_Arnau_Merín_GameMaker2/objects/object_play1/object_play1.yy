{
    "id": "c1d4210d-c9d0-49a6-877b-c86f81e287ab",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_play1",
    "eventList": [
        {
            "id": "11300f46-6893-44a6-be11-1f6a8cc5906c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c1d4210d-c9d0-49a6-877b-c86f81e287ab"
        },
        {
            "id": "9e6d872b-32ce-49c7-8b79-104f396fb8e8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c1d4210d-c9d0-49a6-877b-c86f81e287ab"
        },
        {
            "id": "a1b905a1-27c6-4192-a2f4-a95683cc06b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c1d4210d-c9d0-49a6-877b-c86f81e287ab"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "3f1933f8-d598-406f-9fb0-0ed16030d264",
    "visible": true
}