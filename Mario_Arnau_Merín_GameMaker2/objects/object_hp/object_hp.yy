{
    "id": "501bddfc-0b5e-4285-9d0c-2427e4aa42e0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_hp",
    "eventList": [
        {
            "id": "2b139e9c-ee69-4a68-814b-081f3762b79d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "501bddfc-0b5e-4285-9d0c-2427e4aa42e0"
        },
        {
            "id": "235dad58-8234-474d-ba0a-c8740110ad4d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "501bddfc-0b5e-4285-9d0c-2427e4aa42e0"
        },
        {
            "id": "a1219da7-be49-41e9-af24-ba00d0f29d64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": true,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "501bddfc-0b5e-4285-9d0c-2427e4aa42e0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9033d5a1-8352-4ed1-b1f5-092b122e9ffc",
    "visible": true
}