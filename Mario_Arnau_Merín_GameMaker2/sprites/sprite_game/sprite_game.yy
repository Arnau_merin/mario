{
    "id": "c28087b1-5e40-4e10-b13f-6ab3dc5ba334",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_game",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 125,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "03e01ad7-9e9e-4de7-83b0-098a649e4134",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c28087b1-5e40-4e10-b13f-6ab3dc5ba334",
            "compositeImage": {
                "id": "91de6dc3-fef8-42ac-8acb-fc0c6ecf2aa1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "03e01ad7-9e9e-4de7-83b0-098a649e4134",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04732c23-ed64-4493-bcb5-53073f37427b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "03e01ad7-9e9e-4de7-83b0-098a649e4134",
                    "LayerId": "4fd46080-e096-4cfb-b0db-a2d425151f49"
                }
            ]
        },
        {
            "id": "9b56508e-428b-4635-b4de-f9a123f300f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c28087b1-5e40-4e10-b13f-6ab3dc5ba334",
            "compositeImage": {
                "id": "93143cad-afc0-4055-a968-458ea9a82eb5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9b56508e-428b-4635-b4de-f9a123f300f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "94e3960e-f134-406e-88e5-509bd15c3193",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9b56508e-428b-4635-b4de-f9a123f300f3",
                    "LayerId": "4fd46080-e096-4cfb-b0db-a2d425151f49"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4fd46080-e096-4cfb-b0db-a2d425151f49",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c28087b1-5e40-4e10-b13f-6ab3dc5ba334",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 0,
    "yorig": 0
}