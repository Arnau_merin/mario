{
    "id": "82ee3de4-62ea-4e62-80bc-03314edbd03a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite161",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c7f5c9ca-2503-4ef2-b14a-a3520caa5f78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82ee3de4-62ea-4e62-80bc-03314edbd03a",
            "compositeImage": {
                "id": "68dfbc9c-f8d4-490f-9550-9130b02de173",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c7f5c9ca-2503-4ef2-b14a-a3520caa5f78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b86132f0-196a-4bf4-a927-d687244b0343",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c7f5c9ca-2503-4ef2-b14a-a3520caa5f78",
                    "LayerId": "73f48326-2b3b-4fee-91b0-e780b710585c"
                }
            ]
        },
        {
            "id": "39e2d3a9-b5a9-494f-b300-64afc446c7b2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82ee3de4-62ea-4e62-80bc-03314edbd03a",
            "compositeImage": {
                "id": "697ea44d-5c26-4fbf-94b6-eae0ed15935e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39e2d3a9-b5a9-494f-b300-64afc446c7b2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e525eca2-e579-4485-bde9-ca6822bb4e64",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39e2d3a9-b5a9-494f-b300-64afc446c7b2",
                    "LayerId": "73f48326-2b3b-4fee-91b0-e780b710585c"
                }
            ]
        },
        {
            "id": "e5a80e8b-b13c-4107-a4c8-3e5ce54e950c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82ee3de4-62ea-4e62-80bc-03314edbd03a",
            "compositeImage": {
                "id": "49669ca0-976f-4f72-ac9f-fc4193d3e3fb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5a80e8b-b13c-4107-a4c8-3e5ce54e950c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3eec9af7-c263-4fe1-9b9f-9991cf4ebddd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5a80e8b-b13c-4107-a4c8-3e5ce54e950c",
                    "LayerId": "73f48326-2b3b-4fee-91b0-e780b710585c"
                }
            ]
        },
        {
            "id": "0bddb924-7a83-4dd3-8c05-30e7bab729e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82ee3de4-62ea-4e62-80bc-03314edbd03a",
            "compositeImage": {
                "id": "0e470ead-2719-4264-869b-4277b341541e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0bddb924-7a83-4dd3-8c05-30e7bab729e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ebf0150-2135-4737-bf39-a580c4f104cf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0bddb924-7a83-4dd3-8c05-30e7bab729e1",
                    "LayerId": "73f48326-2b3b-4fee-91b0-e780b710585c"
                }
            ]
        },
        {
            "id": "7d5e3617-df0d-45a8-a974-60a8551a94e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82ee3de4-62ea-4e62-80bc-03314edbd03a",
            "compositeImage": {
                "id": "405d3f42-0170-41cc-aeea-963a33a11f9e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d5e3617-df0d-45a8-a974-60a8551a94e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "af350f82-0bd6-4ea3-bb95-5abace1d7f84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d5e3617-df0d-45a8-a974-60a8551a94e7",
                    "LayerId": "73f48326-2b3b-4fee-91b0-e780b710585c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "73f48326-2b3b-4fee-91b0-e780b710585c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82ee3de4-62ea-4e62-80bc-03314edbd03a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 6,
    "yorig": 8
}