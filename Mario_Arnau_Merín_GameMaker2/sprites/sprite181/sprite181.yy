{
    "id": "27ae2eb5-8b24-4507-a2fe-fbc8eb5b5363",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite181",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 16,
    "bbox_right": 63,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5ad61598-50d3-40b6-88c6-fc6b0ab9588f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27ae2eb5-8b24-4507-a2fe-fbc8eb5b5363",
            "compositeImage": {
                "id": "1d2247be-900c-4761-a100-7013084e4892",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ad61598-50d3-40b6-88c6-fc6b0ab9588f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc5203ac-0a23-4e1d-981f-8aafe06cdda5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ad61598-50d3-40b6-88c6-fc6b0ab9588f",
                    "LayerId": "34bfb243-d2f9-499e-96ed-5de512fa9353"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "34bfb243-d2f9-499e-96ed-5de512fa9353",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27ae2eb5-8b24-4507-a2fe-fbc8eb5b5363",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 19,
    "yorig": 11
}