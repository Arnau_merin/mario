{
    "id": "dbd6933e-3c32-48ab-949d-fef16d705f08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_enemy_lvl21",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "effed224-9c5b-45ec-9463-06c337376b64",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbd6933e-3c32-48ab-949d-fef16d705f08",
            "compositeImage": {
                "id": "5647b640-27ec-4ecd-97f3-ab6cc959d42f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "effed224-9c5b-45ec-9463-06c337376b64",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d6004307-1104-4787-b23d-96f9593e197c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "effed224-9c5b-45ec-9463-06c337376b64",
                    "LayerId": "8e81d19d-6b2c-4510-9673-2626dc161b97"
                }
            ]
        },
        {
            "id": "b541aeb8-fa4d-4082-b203-1f7e5f2f5625",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "dbd6933e-3c32-48ab-949d-fef16d705f08",
            "compositeImage": {
                "id": "9569450d-9f1b-4349-ab67-3d28937f20cc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b541aeb8-fa4d-4082-b203-1f7e5f2f5625",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1ec791a-72d3-4c37-a7b7-4725dcaa56d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b541aeb8-fa4d-4082-b203-1f7e5f2f5625",
                    "LayerId": "8e81d19d-6b2c-4510-9673-2626dc161b97"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8e81d19d-6b2c-4510-9673-2626dc161b97",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "dbd6933e-3c32-48ab-949d-fef16d705f08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}