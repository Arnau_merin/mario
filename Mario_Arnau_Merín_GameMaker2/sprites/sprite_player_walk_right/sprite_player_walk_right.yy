{
    "id": "03649708-957a-4215-b8d6-fa8061f19d7c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_walk_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "267419c7-1b7f-44ea-8a1c-715300a232b0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03649708-957a-4215-b8d6-fa8061f19d7c",
            "compositeImage": {
                "id": "7aa320f9-df26-4c8f-a571-fd7ba4a1c1dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "267419c7-1b7f-44ea-8a1c-715300a232b0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27522ebf-206e-45ab-b6b8-4a894fa4a817",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "267419c7-1b7f-44ea-8a1c-715300a232b0",
                    "LayerId": "31c17c4b-6d83-45c5-a4e4-1a4012cf8fa8"
                }
            ]
        },
        {
            "id": "d6e898cc-09f6-46c7-94f5-64a879a6c73c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03649708-957a-4215-b8d6-fa8061f19d7c",
            "compositeImage": {
                "id": "128cf3f8-1c97-4b76-a989-6fe78efcecb7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d6e898cc-09f6-46c7-94f5-64a879a6c73c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0da31b39-fc06-4b9b-86e1-bee94c84a5ba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d6e898cc-09f6-46c7-94f5-64a879a6c73c",
                    "LayerId": "31c17c4b-6d83-45c5-a4e4-1a4012cf8fa8"
                }
            ]
        },
        {
            "id": "e41eca18-6538-4ed0-923b-d149893e89a4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03649708-957a-4215-b8d6-fa8061f19d7c",
            "compositeImage": {
                "id": "925b68de-5177-412b-aa8d-ec0be0af02eb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e41eca18-6538-4ed0-923b-d149893e89a4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bcb3a0a-4e1c-4354-9dd7-64be736574c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e41eca18-6538-4ed0-923b-d149893e89a4",
                    "LayerId": "31c17c4b-6d83-45c5-a4e4-1a4012cf8fa8"
                }
            ]
        },
        {
            "id": "0348792e-14b0-41a7-8fc0-7779ce835fb1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "03649708-957a-4215-b8d6-fa8061f19d7c",
            "compositeImage": {
                "id": "5a90cefb-34fe-4ec7-beee-69b597b16e1c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0348792e-14b0-41a7-8fc0-7779ce835fb1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3643d19-8b89-42ef-a410-8aa563c37f09",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0348792e-14b0-41a7-8fc0-7779ce835fb1",
                    "LayerId": "31c17c4b-6d83-45c5-a4e4-1a4012cf8fa8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "31c17c4b-6d83-45c5-a4e4-1a4012cf8fa8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "03649708-957a-4215-b8d6-fa8061f19d7c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 8,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}