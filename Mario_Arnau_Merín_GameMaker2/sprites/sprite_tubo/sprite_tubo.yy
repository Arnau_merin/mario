{
    "id": "e882d06c-c3cb-473e-92d3-b02d4e5ff6fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_tubo",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 32,
    "bbox_right": 63,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "68a62ff6-8547-4092-bf66-e529d25f6fc3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e882d06c-c3cb-473e-92d3-b02d4e5ff6fc",
            "compositeImage": {
                "id": "53d7830b-bbff-4793-a1f1-2190ccbd2a77",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "68a62ff6-8547-4092-bf66-e529d25f6fc3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6c98535f-db77-4f57-96ed-f1f7b8903473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "68a62ff6-8547-4092-bf66-e529d25f6fc3",
                    "LayerId": "df9cb851-8616-4b40-a75c-995656746c84"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "df9cb851-8616-4b40-a75c-995656746c84",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e882d06c-c3cb-473e-92d3-b02d4e5ff6fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}