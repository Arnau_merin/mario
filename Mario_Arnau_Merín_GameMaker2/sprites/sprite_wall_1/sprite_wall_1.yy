{
    "id": "3af43a35-9fab-4140-b143-17a5479a157b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall_1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7b128283-28d1-4206-adbd-3e227e4373e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3af43a35-9fab-4140-b143-17a5479a157b",
            "compositeImage": {
                "id": "5bac3a5b-d417-4012-8b2f-045b4af2a05c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7b128283-28d1-4206-adbd-3e227e4373e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "56e6671c-105a-4219-9fbb-33299e15ae16",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7b128283-28d1-4206-adbd-3e227e4373e1",
                    "LayerId": "9e9e8933-5e09-48cc-8bab-763804b213e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9e9e8933-5e09-48cc-8bab-763804b213e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3af43a35-9fab-4140-b143-17a5479a157b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 16
}