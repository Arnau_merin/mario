{
    "id": "7a877a99-3f48-46a2-9293-eba0bbbdf445",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall12",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d0e687b4-91dc-47a4-9684-9c60d6c29149",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7a877a99-3f48-46a2-9293-eba0bbbdf445",
            "compositeImage": {
                "id": "4eb74efa-98c4-4f79-9ff4-1e41946bebd4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0e687b4-91dc-47a4-9684-9c60d6c29149",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "658ef63c-5ca9-4358-8063-b02e9875e4b9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0e687b4-91dc-47a4-9684-9c60d6c29149",
                    "LayerId": "e9bef107-fd88-4dde-a9ec-dc8b3ad013e2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "e9bef107-fd88-4dde-a9ec-dc8b3ad013e2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7a877a99-3f48-46a2-9293-eba0bbbdf445",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}