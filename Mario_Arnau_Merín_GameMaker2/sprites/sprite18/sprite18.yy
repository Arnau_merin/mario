{
    "id": "2a5fa9d6-29f0-48cf-aa2c-9a01f4d84d08",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite18",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "65f1258a-b242-492b-9fd0-1d40ccfe5cde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2a5fa9d6-29f0-48cf-aa2c-9a01f4d84d08",
            "compositeImage": {
                "id": "2227971e-f3b1-473f-a689-fc522a05c907",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "65f1258a-b242-492b-9fd0-1d40ccfe5cde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2ed5f4f7-e6ab-4e56-9fe7-8bc51ad5f3e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "65f1258a-b242-492b-9fd0-1d40ccfe5cde",
                    "LayerId": "f3315f8d-8f4f-4f11-9f62-e4eab1189b89"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "f3315f8d-8f4f-4f11-9f62-e4eab1189b89",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2a5fa9d6-29f0-48cf-aa2c-9a01f4d84d08",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 46,
    "yorig": 12
}