{
    "id": "e1a72973-d5e0-47f9-aa3e-359746003878",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_tubo1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 14,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "eae50fae-be6f-4353-8464-a221d1999c95",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e1a72973-d5e0-47f9-aa3e-359746003878",
            "compositeImage": {
                "id": "826d39c2-522b-425c-ac68-0450117faaaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eae50fae-be6f-4353-8464-a221d1999c95",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d216a50c-8f12-4e03-9107-887386beda63",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eae50fae-be6f-4353-8464-a221d1999c95",
                    "LayerId": "8d74812d-2cc9-44b0-8e83-422a0bc1b6ac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8d74812d-2cc9-44b0-8e83-422a0bc1b6ac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e1a72973-d5e0-47f9-aa3e-359746003878",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}