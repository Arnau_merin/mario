{
    "id": "0d98fcc9-36f1-4730-abb9-f67ab5c201c4",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite10",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 292,
    "bbox_left": 1,
    "bbox_right": 637,
    "bbox_top": 6,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5e895ec9-9768-4a82-b867-643ada8de4ea",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0d98fcc9-36f1-4730-abb9-f67ab5c201c4",
            "compositeImage": {
                "id": "684ec1db-d56f-497a-8b88-7da4ab095cd1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5e895ec9-9768-4a82-b867-643ada8de4ea",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "62220b18-1918-4699-9a8e-c12f01a46657",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5e895ec9-9768-4a82-b867-643ada8de4ea",
                    "LayerId": "e035c371-84cd-4d1a-8909-fdc458d33b50"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 293,
    "layers": [
        {
            "id": "e035c371-84cd-4d1a-8909-fdc458d33b50",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0d98fcc9-36f1-4730-abb9-f67ab5c201c4",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 640,
    "xorig": 0,
    "yorig": 0
}