{
    "id": "9033d5a1-8352-4ed1-b1f5-092b122e9ffc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_hp",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 5,
    "bbox_left": 0,
    "bbox_right": 27,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d89165fa-253c-4592-a693-02b013634208",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9033d5a1-8352-4ed1-b1f5-092b122e9ffc",
            "compositeImage": {
                "id": "7e21bf65-8f39-402a-ba54-07533ae5b7ed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d89165fa-253c-4592-a693-02b013634208",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16ec2faf-9e5e-4141-9ee1-416fc6b15989",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d89165fa-253c-4592-a693-02b013634208",
                    "LayerId": "4e9270fc-f53e-49eb-940e-c4c7a0d0a90b"
                }
            ]
        },
        {
            "id": "c16e44be-659e-4b0c-a6d6-839e53bfe396",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9033d5a1-8352-4ed1-b1f5-092b122e9ffc",
            "compositeImage": {
                "id": "08e10d13-47f4-4cb3-bda5-410451117d8d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c16e44be-659e-4b0c-a6d6-839e53bfe396",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f797ab08-0f79-4df5-ac89-d34898fd73e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c16e44be-659e-4b0c-a6d6-839e53bfe396",
                    "LayerId": "4e9270fc-f53e-49eb-940e-c4c7a0d0a90b"
                }
            ]
        },
        {
            "id": "1994388f-9e66-4b0b-97c9-0a0344c5bd5e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9033d5a1-8352-4ed1-b1f5-092b122e9ffc",
            "compositeImage": {
                "id": "9f121d76-6e16-459a-854f-f340892f83db",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1994388f-9e66-4b0b-97c9-0a0344c5bd5e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b005076a-3398-4920-9d4a-1bf830e36133",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1994388f-9e66-4b0b-97c9-0a0344c5bd5e",
                    "LayerId": "4e9270fc-f53e-49eb-940e-c4c7a0d0a90b"
                }
            ]
        },
        {
            "id": "4ee79614-8a9b-467a-b5a9-561c9c10614d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9033d5a1-8352-4ed1-b1f5-092b122e9ffc",
            "compositeImage": {
                "id": "2f8d26d4-7b10-4cb5-ad9d-52efb909c333",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee79614-8a9b-467a-b5a9-561c9c10614d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d566ec5-d3f1-4c96-8875-0fba320362a8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee79614-8a9b-467a-b5a9-561c9c10614d",
                    "LayerId": "4e9270fc-f53e-49eb-940e-c4c7a0d0a90b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "4e9270fc-f53e-49eb-940e-c4c7a0d0a90b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9033d5a1-8352-4ed1-b1f5-092b122e9ffc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 13,
    "yorig": 3
}