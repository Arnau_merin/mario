{
    "id": "ce0e938f-93d3-4206-b865-987a0269d9bc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite16",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 1,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "07b6d4f6-7971-46a8-8dfc-dc48b21871fc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce0e938f-93d3-4206-b865-987a0269d9bc",
            "compositeImage": {
                "id": "41268d4b-7d4c-4c05-8a27-c4537cf60d8c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07b6d4f6-7971-46a8-8dfc-dc48b21871fc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d19348a5-a726-453b-9012-7f8ae52ad351",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07b6d4f6-7971-46a8-8dfc-dc48b21871fc",
                    "LayerId": "f558303f-7521-464c-b824-aa8fc82487b1"
                }
            ]
        },
        {
            "id": "ada49b82-c618-496c-b465-58dced5b5f9e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce0e938f-93d3-4206-b865-987a0269d9bc",
            "compositeImage": {
                "id": "54cd90f0-6b5e-4591-9cf3-4cb96e49349d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ada49b82-c618-496c-b465-58dced5b5f9e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8aaad100-51ee-4d62-abeb-081062b3bae0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ada49b82-c618-496c-b465-58dced5b5f9e",
                    "LayerId": "f558303f-7521-464c-b824-aa8fc82487b1"
                }
            ]
        },
        {
            "id": "e5c8b64a-77a7-4d47-9179-bbaf96d8b3e3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce0e938f-93d3-4206-b865-987a0269d9bc",
            "compositeImage": {
                "id": "9f7afae8-2737-4aa0-968e-1ebd2038afe3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e5c8b64a-77a7-4d47-9179-bbaf96d8b3e3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "de99b278-35aa-41fb-9789-07d42c29010b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e5c8b64a-77a7-4d47-9179-bbaf96d8b3e3",
                    "LayerId": "f558303f-7521-464c-b824-aa8fc82487b1"
                }
            ]
        },
        {
            "id": "eba874da-2423-4e2f-9087-6b2462ca06ed",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce0e938f-93d3-4206-b865-987a0269d9bc",
            "compositeImage": {
                "id": "5f565525-50d4-4678-bd84-28a77aa43a24",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "eba874da-2423-4e2f-9087-6b2462ca06ed",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d98401a-e556-41e1-9738-9b921b0a463f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "eba874da-2423-4e2f-9087-6b2462ca06ed",
                    "LayerId": "f558303f-7521-464c-b824-aa8fc82487b1"
                }
            ]
        },
        {
            "id": "7f01a9b1-6a2b-4150-ad70-9965ebe6b82f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ce0e938f-93d3-4206-b865-987a0269d9bc",
            "compositeImage": {
                "id": "fbaf8ce6-ca49-4f9b-95a0-fe7a27eda866",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7f01a9b1-6a2b-4150-ad70-9965ebe6b82f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b5fe478d-e76d-4cee-9057-ae767babf3e8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7f01a9b1-6a2b-4150-ad70-9965ebe6b82f",
                    "LayerId": "f558303f-7521-464c-b824-aa8fc82487b1"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f558303f-7521-464c-b824-aa8fc82487b1",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ce0e938f-93d3-4206-b865-987a0269d9bc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 6,
    "yorig": 8
}