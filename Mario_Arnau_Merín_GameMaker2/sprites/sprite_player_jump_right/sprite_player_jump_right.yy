{
    "id": "ea1cb5f0-1506-4075-904a-7991c21c74fc",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_jump_right",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3388f798-8e4d-4416-88de-adc004ca6e7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ea1cb5f0-1506-4075-904a-7991c21c74fc",
            "compositeImage": {
                "id": "4031e3ec-6d7c-4866-8c85-8ea76735c413",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3388f798-8e4d-4416-88de-adc004ca6e7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bfd061b7-da3b-48d4-9e69-c98211d151bf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3388f798-8e4d-4416-88de-adc004ca6e7a",
                    "LayerId": "ca9ec7b4-3657-4862-a4a4-1df698e48bce"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "ca9ec7b4-3657-4862-a4a4-1df698e48bce",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ea1cb5f0-1506-4075-904a-7991c21c74fc",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}