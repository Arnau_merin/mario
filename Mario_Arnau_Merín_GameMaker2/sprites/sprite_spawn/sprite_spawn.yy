{
    "id": "bde7693c-2384-46e8-9fe3-40c777559289",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_spawn",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "bc338287-b9f3-4f85-b1e5-1929c159104b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde7693c-2384-46e8-9fe3-40c777559289",
            "compositeImage": {
                "id": "efbc0520-c5ea-4815-92e3-58371efc4f5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bc338287-b9f3-4f85-b1e5-1929c159104b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d88b97aa-fa6a-4dd2-b782-a39dc151669f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bc338287-b9f3-4f85-b1e5-1929c159104b",
                    "LayerId": "dcc34b34-00c1-4e6f-b07e-0c9f593f33b2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "dcc34b34-00c1-4e6f-b07e-0c9f593f33b2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bde7693c-2384-46e8-9fe3-40c777559289",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}