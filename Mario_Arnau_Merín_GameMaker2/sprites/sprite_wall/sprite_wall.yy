{
    "id": "47715d92-7e5c-4699-963b-fd5f677987e8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_wall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d272b1b5-7a6f-479a-afdc-954a51d4717d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "47715d92-7e5c-4699-963b-fd5f677987e8",
            "compositeImage": {
                "id": "51113611-2732-4c69-a50b-bedf9807f92b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d272b1b5-7a6f-479a-afdc-954a51d4717d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3acfa6d-5131-4189-abca-aef219a69fa0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d272b1b5-7a6f-479a-afdc-954a51d4717d",
                    "LayerId": "46fb9eeb-270a-43da-8f23-aa980ce49bed"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "46fb9eeb-270a-43da-8f23-aa980ce49bed",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "47715d92-7e5c-4699-963b-fd5f677987e8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}