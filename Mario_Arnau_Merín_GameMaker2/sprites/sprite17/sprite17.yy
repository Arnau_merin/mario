{
    "id": "153f82e8-70ce-4442-b0bd-95221414dc0d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite17",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 24,
    "bbox_right": 63,
    "bbox_top": 30,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb1c78b9-cdc3-4fcb-ac71-985ddc7c55c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "153f82e8-70ce-4442-b0bd-95221414dc0d",
            "compositeImage": {
                "id": "1a784a0f-45f5-409a-ac19-79c8491f6cbd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb1c78b9-cdc3-4fcb-ac71-985ddc7c55c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9c069bd-0bfd-4070-a69d-05b29c7e95b0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb1c78b9-cdc3-4fcb-ac71-985ddc7c55c5",
                    "LayerId": "eb48100c-b157-4e3e-b5ff-d9137a3f7aac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "eb48100c-b157-4e3e-b5ff-d9137a3f7aac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "153f82e8-70ce-4442-b0bd-95221414dc0d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}