{
    "id": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_fall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5c5ad7ee-5be2-464d-9cec-63009201e8a1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "afb89856-d7be-4b43-bf7e-f0175076a831",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5c5ad7ee-5be2-464d-9cec-63009201e8a1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0004fa0e-7d3c-4304-a598-eec145838e84",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5c5ad7ee-5be2-464d-9cec-63009201e8a1",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "2a0cbaf4-f149-48e1-9623-45c6fdd18de4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "a81ad57a-304b-4ed1-9b43-326fa285713e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2a0cbaf4-f149-48e1-9623-45c6fdd18de4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "78690913-67cc-411e-ae06-2192f8c7ceca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2a0cbaf4-f149-48e1-9623-45c6fdd18de4",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "09da010e-6eaa-4ccc-bb49-39920a160d78",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "61172f05-6eb1-44ff-84b6-e56b09d38382",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "09da010e-6eaa-4ccc-bb49-39920a160d78",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6fab2b41-40c9-4eaa-b3e0-80e656718bf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "09da010e-6eaa-4ccc-bb49-39920a160d78",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "4b8190ce-b707-4a6c-a725-b9896b602ff7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "a5a75f6c-4861-4150-9693-1e241038adc2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4b8190ce-b707-4a6c-a725-b9896b602ff7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8d14d074-a5dc-4300-a0d2-5710db09b0d4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4b8190ce-b707-4a6c-a725-b9896b602ff7",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "b049d245-4487-4c3d-864e-fac733777d1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "ade083ed-90ed-4c77-b60a-52d9f5f39650",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b049d245-4487-4c3d-864e-fac733777d1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c629daa2-dd8f-4de2-b574-83d1e17fbdcc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b049d245-4487-4c3d-864e-fac733777d1e",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "061dbc5a-9c4f-4323-8134-98fbf78f7a2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "e04addf5-b68b-4896-8fee-df6e0e87796f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "061dbc5a-9c4f-4323-8134-98fbf78f7a2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cee76b49-9c3c-4abc-b1e1-f6b8d217229a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "061dbc5a-9c4f-4323-8134-98fbf78f7a2e",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "70d3ee84-a8f3-4fc3-9ad1-3f88eae8f517",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "eca41284-5daa-4f94-8a03-eb556eb4420c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "70d3ee84-a8f3-4fc3-9ad1-3f88eae8f517",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "500cfa60-5156-4049-a6f3-830c27ddef20",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "70d3ee84-a8f3-4fc3-9ad1-3f88eae8f517",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "cee8b146-a311-4099-8f7b-783ece98aa12",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "6eab28b4-b2e1-4f32-aeb1-1a65740c870e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cee8b146-a311-4099-8f7b-783ece98aa12",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "77f6ac90-2848-429a-b0bb-dcd7832b868f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cee8b146-a311-4099-8f7b-783ece98aa12",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "c47df035-14d9-449c-9794-851d14a170cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "8febc990-39aa-4cd1-970d-a7a475153825",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c47df035-14d9-449c-9794-851d14a170cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c9a34f83-b465-4b74-9efd-a257b44cc56b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c47df035-14d9-449c-9794-851d14a170cd",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "f7b25a29-aa57-48ac-b86d-ba97e0ba4b82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "863f35da-78e9-407b-9ad8-3c04b7e23edf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7b25a29-aa57-48ac-b86d-ba97e0ba4b82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cda45e7-fc31-49b3-8266-eef5a50c8e6f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7b25a29-aa57-48ac-b86d-ba97e0ba4b82",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "37b1c463-bc99-45c6-9cf5-524e31e1d5b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "87a9c33a-e5f5-4927-b139-1f19e31ebcb1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "37b1c463-bc99-45c6-9cf5-524e31e1d5b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a69f860-978e-40f7-811d-b45e238c285b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "37b1c463-bc99-45c6-9cf5-524e31e1d5b1",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "f91f0479-a5ad-406d-ac67-320f092c291b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "11ccc0e4-1976-4706-bc2d-4d756bda6b67",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f91f0479-a5ad-406d-ac67-320f092c291b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8f5aff66-d019-41a9-872e-0eb2dca93e17",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f91f0479-a5ad-406d-ac67-320f092c291b",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "d46eee14-5aea-4712-b33d-f4baacb0ed49",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "96bf9cea-cbe5-4d37-aa30-26cb4de02ce9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d46eee14-5aea-4712-b33d-f4baacb0ed49",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "33078cf2-8819-4e2f-ba88-cbbc9ccd08a6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d46eee14-5aea-4712-b33d-f4baacb0ed49",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "a6fd8408-9dab-4eb7-a7a6-c67f24cde2b1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "4b554aeb-03dc-4bd1-a3a8-36dc20efa67d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a6fd8408-9dab-4eb7-a7a6-c67f24cde2b1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c9e7e9e-f960-4851-92e2-fd0ed31b4b06",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a6fd8408-9dab-4eb7-a7a6-c67f24cde2b1",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "481c5007-7b6b-4aaa-b715-fcab2cd58992",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "9798e334-45ba-4b89-ba1b-d72c2cfeb475",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "481c5007-7b6b-4aaa-b715-fcab2cd58992",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1a75c58e-356e-449b-802b-8537b6eae43e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "481c5007-7b6b-4aaa-b715-fcab2cd58992",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "f219087a-6360-4c9a-9575-c3e38c0b2def",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "a702d33a-2bf7-41f8-abe8-6c18f8f5bcd6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f219087a-6360-4c9a-9575-c3e38c0b2def",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29fdb48c-425e-45c2-adca-44e26f424fe4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f219087a-6360-4c9a-9575-c3e38c0b2def",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "b507061e-55ab-4a33-9340-b28cab25fc20",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "066821b0-3e7e-42b7-be16-167eb4e72234",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b507061e-55ab-4a33-9340-b28cab25fc20",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cddd5a41-8cdb-410b-9e93-1d552d652097",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b507061e-55ab-4a33-9340-b28cab25fc20",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "7358376a-83d8-4a6d-8f9f-3e1cdd629971",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "5c3d297c-a835-4e09-85ef-eee1728aee29",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7358376a-83d8-4a6d-8f9f-3e1cdd629971",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "750ae3a4-ebb0-43d2-b1a3-0c8665ca525e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7358376a-83d8-4a6d-8f9f-3e1cdd629971",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "2ced63db-1270-4f7b-a364-186a4c3f4bfb",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "3bfdf23f-7b95-4087-97f7-835ff37d2b3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ced63db-1270-4f7b-a364-186a4c3f4bfb",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "82e68a33-f174-4a1b-a332-3a06249230d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ced63db-1270-4f7b-a364-186a4c3f4bfb",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "aa1c82ef-801b-40ed-8fd7-9bff73478c8a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "8a75950c-9d03-429e-a657-ba32e9170035",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa1c82ef-801b-40ed-8fd7-9bff73478c8a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9714c9e-2fd4-4fcc-9c18-67dc18a95383",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa1c82ef-801b-40ed-8fd7-9bff73478c8a",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "563aa2ce-2f63-4232-aaaa-4bc9cfec940c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "1e98e7d1-16c8-4cf2-bbe9-51c539815973",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "563aa2ce-2f63-4232-aaaa-4bc9cfec940c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6623ed78-45c1-45da-92f9-fb54d4d00265",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "563aa2ce-2f63-4232-aaaa-4bc9cfec940c",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "90de84b7-8373-414a-a8ee-6b8036ddfad8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "0c470c83-7e19-440f-8ea4-44b448c7f577",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "90de84b7-8373-414a-a8ee-6b8036ddfad8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f93d6a31-3ffd-46de-8f0c-fd8b9e24fcba",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "90de84b7-8373-414a-a8ee-6b8036ddfad8",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        },
        {
            "id": "4c5859bd-858e-4acf-a52e-410ebfab0aba",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "compositeImage": {
                "id": "7c4b422a-ef26-4a69-b8b7-4b9d41535505",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4c5859bd-858e-4acf-a52e-410ebfab0aba",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "795fb4d1-ad8a-4fa7-93e4-bd82dc860c77",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4c5859bd-858e-4acf-a52e-410ebfab0aba",
                    "LayerId": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "95ec45f1-f912-4f53-bbfc-4ec7be3da01c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "8866ac6c-a2b3-4cf0-86a3-8aa9612ce1ab",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}