{
    "id": "5155c68f-4959-4cb7-94a2-7086a8e8084a",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_menu",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 299,
    "bbox_left": 0,
    "bbox_right": 269,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "db5825e3-62ef-4c20-bbd7-130e21680745",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5155c68f-4959-4cb7-94a2-7086a8e8084a",
            "compositeImage": {
                "id": "e96367b6-01ec-44a6-8476-e651b1034c39",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "db5825e3-62ef-4c20-bbd7-130e21680745",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3ccbc96b-08c3-45ae-b003-56a4ae55890c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "db5825e3-62ef-4c20-bbd7-130e21680745",
                    "LayerId": "5f30be1e-4397-4e71-9885-143767e552c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 300,
    "layers": [
        {
            "id": "5f30be1e-4397-4e71-9885-143767e552c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5155c68f-4959-4cb7-94a2-7086a8e8084a",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 270,
    "xorig": 0,
    "yorig": 0
}