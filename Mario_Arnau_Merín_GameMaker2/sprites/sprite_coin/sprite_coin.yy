{
    "id": "4a0e589f-002a-4721-be13-9def464a28ca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_coin",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 14,
    "bbox_left": 7,
    "bbox_right": 14,
    "bbox_top": 4,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e3842e2a-b566-4be0-8ab4-ed3ca6c1c47f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0e589f-002a-4721-be13-9def464a28ca",
            "compositeImage": {
                "id": "f0567855-beff-4f71-8cb1-47c145c78e95",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e3842e2a-b566-4be0-8ab4-ed3ca6c1c47f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "25ef0008-5413-406d-886a-be977a8059ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e3842e2a-b566-4be0-8ab4-ed3ca6c1c47f",
                    "LayerId": "cbdf0f02-e0e4-4e8e-b69d-f848d5262a4b"
                }
            ]
        },
        {
            "id": "6e3b433e-8bfd-453f-8a97-a590e5987981",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0e589f-002a-4721-be13-9def464a28ca",
            "compositeImage": {
                "id": "3c0464db-a755-4f1a-86a7-5eb0b6ea680c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6e3b433e-8bfd-453f-8a97-a590e5987981",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "918a154f-8318-4912-8ac4-f25da8bec62f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6e3b433e-8bfd-453f-8a97-a590e5987981",
                    "LayerId": "cbdf0f02-e0e4-4e8e-b69d-f848d5262a4b"
                }
            ]
        },
        {
            "id": "41964dfc-bc87-497a-8c49-ef6e2c51fcd0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4a0e589f-002a-4721-be13-9def464a28ca",
            "compositeImage": {
                "id": "60985d40-9fd3-4764-b63a-9ef18394d200",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41964dfc-bc87-497a-8c49-ef6e2c51fcd0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19177f05-03a8-4cfa-8b08-e237fdcf85c8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41964dfc-bc87-497a-8c49-ef6e2c51fcd0",
                    "LayerId": "cbdf0f02-e0e4-4e8e-b69d-f848d5262a4b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "cbdf0f02-e0e4-4e8e-b69d-f848d5262a4b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4a0e589f-002a-4721-be13-9def464a28ca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 6,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": -7,
    "yorig": 13
}