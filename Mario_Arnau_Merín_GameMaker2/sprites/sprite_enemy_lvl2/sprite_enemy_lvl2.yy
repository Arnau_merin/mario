{
    "id": "0f40999b-c806-4798-809b-7149ab6799c2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_enemy_lvl2",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 3,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "713c60ed-0e90-4a6d-8c56-693fcbfc4616",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f40999b-c806-4798-809b-7149ab6799c2",
            "compositeImage": {
                "id": "6de98b74-afab-4d69-a5b6-9d223266e3e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "713c60ed-0e90-4a6d-8c56-693fcbfc4616",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f924c1b8-8903-4cb4-9a51-d8ed7b955c81",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "713c60ed-0e90-4a6d-8c56-693fcbfc4616",
                    "LayerId": "4d55ed68-86a0-4bf7-b2bf-2baf5331a089"
                }
            ]
        },
        {
            "id": "e0144d65-ea34-4d25-af6f-86edfebe54b8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0f40999b-c806-4798-809b-7149ab6799c2",
            "compositeImage": {
                "id": "ed2220d7-064e-457a-82b0-27f116cf48e2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e0144d65-ea34-4d25-af6f-86edfebe54b8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b84a825-51fd-4e53-80bc-21524c3dc275",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e0144d65-ea34-4d25-af6f-86edfebe54b8",
                    "LayerId": "4d55ed68-86a0-4bf7-b2bf-2baf5331a089"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "4d55ed68-86a0-4bf7-b2bf-2baf5331a089",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0f40999b-c806-4798-809b-7149ab6799c2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 3,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}