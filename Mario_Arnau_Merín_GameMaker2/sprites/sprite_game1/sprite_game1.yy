{
    "id": "3f1933f8-d598-406f-9fb0-0ed16030d264",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_game1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 125,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8311faf5-2042-4dac-95a2-3989a5aedc82",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1933f8-d598-406f-9fb0-0ed16030d264",
            "compositeImage": {
                "id": "a4782b19-bc54-42d7-b549-c8743007b46f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8311faf5-2042-4dac-95a2-3989a5aedc82",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "849996a3-52d5-4867-baf2-ca121b24869e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8311faf5-2042-4dac-95a2-3989a5aedc82",
                    "LayerId": "f37dc2ee-9d6f-4dd3-a445-ca9cf6cec496"
                }
            ]
        },
        {
            "id": "bbe2cb87-9c5b-42f9-b158-935112b39484",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3f1933f8-d598-406f-9fb0-0ed16030d264",
            "compositeImage": {
                "id": "6da06b4e-8dde-46b5-bc74-e54d0355016e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "bbe2cb87-9c5b-42f9-b158-935112b39484",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "45634de4-1e4e-45ab-9847-ec690585218a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "bbe2cb87-9c5b-42f9-b158-935112b39484",
                    "LayerId": "f37dc2ee-9d6f-4dd3-a445-ca9cf6cec496"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "f37dc2ee-9d6f-4dd3-a445-ca9cf6cec496",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3f1933f8-d598-406f-9fb0-0ed16030d264",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 126,
    "xorig": 0,
    "yorig": 0
}