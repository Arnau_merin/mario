{
    "id": "c2853a43-3b16-434d-b66f-b0176ed62ca2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_jump",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54624991-85f0-4fbf-b299-edf13d1f638c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c2853a43-3b16-434d-b66f-b0176ed62ca2",
            "compositeImage": {
                "id": "b2541626-2cfa-4004-ba0d-737a48aad5d4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54624991-85f0-4fbf-b299-edf13d1f638c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b59999c7-d389-4fc8-9d43-fd9c230e824f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54624991-85f0-4fbf-b299-edf13d1f638c",
                    "LayerId": "929e4bb8-1fa4-493b-a04b-a33139d6a469"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "929e4bb8-1fa4-493b-a04b-a33139d6a469",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c2853a43-3b16-434d-b66f-b0176ed62ca2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 0,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}