{
    "id": "2e706503-a244-4042-9c27-489e50e522f7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_death",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "41da5818-5093-4611-b609-752c31a53a76",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e706503-a244-4042-9c27-489e50e522f7",
            "compositeImage": {
                "id": "66430baf-4749-46b2-8225-d99a9a90cf53",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "41da5818-5093-4611-b609-752c31a53a76",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b55ee216-a9ab-443e-ab44-b7ea041f8981",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "41da5818-5093-4611-b609-752c31a53a76",
                    "LayerId": "1cf9e1d6-98f9-4cfe-a622-7e50018cd576"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "1cf9e1d6-98f9-4cfe-a622-7e50018cd576",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e706503-a244-4042-9c27-489e50e522f7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}