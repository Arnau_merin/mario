{
    "id": "224d49f8-50f3-4bd1-af1d-869e8d45b496",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_pup",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "63b4fd15-db30-4f20-8251-21622f3cece8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "224d49f8-50f3-4bd1-af1d-869e8d45b496",
            "compositeImage": {
                "id": "58f2619d-53a3-4f8b-91d0-79ae4b6bc050",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "63b4fd15-db30-4f20-8251-21622f3cece8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "566caab1-6d78-42f1-abe4-c4bfb15803a1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "63b4fd15-db30-4f20-8251-21622f3cece8",
                    "LayerId": "6b18801a-24fb-4f76-a539-c107f6878117"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6b18801a-24fb-4f76-a539-c107f6878117",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "224d49f8-50f3-4bd1-af1d-869e8d45b496",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 64,
    "yorig": 32
}