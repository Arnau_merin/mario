{
    "id": "e4786d59-5869-43d3-b554-b90f1e2b88bd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_walk",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 21,
    "bbox_left": 1,
    "bbox_right": 14,
    "bbox_top": 1,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "08327269-76fd-4e7d-aac4-7127e90f7f7a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4786d59-5869-43d3-b554-b90f1e2b88bd",
            "compositeImage": {
                "id": "58a8177a-713f-434d-adfd-0c80c9ebda4c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "08327269-76fd-4e7d-aac4-7127e90f7f7a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c98d3f15-aad0-4f2f-9ca0-73fd5779f877",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "08327269-76fd-4e7d-aac4-7127e90f7f7a",
                    "LayerId": "43dba684-1637-4f22-999f-7c3d4a8640f3"
                }
            ]
        },
        {
            "id": "440cd706-899e-4117-bd8e-d4e70cf5bb0b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4786d59-5869-43d3-b554-b90f1e2b88bd",
            "compositeImage": {
                "id": "aab2d99d-4037-46e9-8653-0005a5756ce3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "440cd706-899e-4117-bd8e-d4e70cf5bb0b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fea8213a-d8df-4ff5-9d05-99cca3ec58ce",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "440cd706-899e-4117-bd8e-d4e70cf5bb0b",
                    "LayerId": "43dba684-1637-4f22-999f-7c3d4a8640f3"
                }
            ]
        },
        {
            "id": "2abcc785-cb50-4d68-ac20-65fc76e2de23",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4786d59-5869-43d3-b554-b90f1e2b88bd",
            "compositeImage": {
                "id": "eba5687c-705a-443d-87e4-092f5d42d807",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2abcc785-cb50-4d68-ac20-65fc76e2de23",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "312ed53c-936c-491b-89ee-e93a0997dede",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2abcc785-cb50-4d68-ac20-65fc76e2de23",
                    "LayerId": "43dba684-1637-4f22-999f-7c3d4a8640f3"
                }
            ]
        },
        {
            "id": "195d90a4-fdcf-4b80-9ff8-64e1275fd7a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e4786d59-5869-43d3-b554-b90f1e2b88bd",
            "compositeImage": {
                "id": "36535407-c491-42f9-98f4-1260344c6613",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "195d90a4-fdcf-4b80-9ff8-64e1275fd7a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "54eb1b00-3511-44dd-995c-3cdef93024d3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "195d90a4-fdcf-4b80-9ff8-64e1275fd7a9",
                    "LayerId": "43dba684-1637-4f22-999f-7c3d4a8640f3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 22,
    "layers": [
        {
            "id": "43dba684-1637-4f22-999f-7c3d4a8640f3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e4786d59-5869-43d3-b554-b90f1e2b88bd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 12,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 11
}