{
    "id": "a7237d55-44e2-4052-aad1-f80d947e1518",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite27",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "dfec33f8-7416-488d-90cd-8776a9a21c5a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a7237d55-44e2-4052-aad1-f80d947e1518",
            "compositeImage": {
                "id": "344ed846-06d9-4d9c-96f9-9dcd72ae9bbb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dfec33f8-7416-488d-90cd-8776a9a21c5a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "10a89250-fd52-471d-9d80-6ae2fe51e209",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dfec33f8-7416-488d-90cd-8776a9a21c5a",
                    "LayerId": "6d0f062b-6cae-4cd0-ae61-fbfacf5dec7f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "6d0f062b-6cae-4cd0-ae61-fbfacf5dec7f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a7237d55-44e2-4052-aad1-f80d947e1518",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 7
}