{
    "id": "e374792b-519a-47e7-b677-0e835e20bc0f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player_fall0",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 183,
    "bbox_left": 88,
    "bbox_right": 101,
    "bbox_top": 163,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0180cfc6-60e9-45a3-9073-2be6d249c3c9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "7f71482a-93d3-4723-8225-3c0c3a960246",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0180cfc6-60e9-45a3-9073-2be6d249c3c9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "29c0e7d0-f49e-4c99-8505-d1d9fa85d9d0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0180cfc6-60e9-45a3-9073-2be6d249c3c9",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "ad008004-ab0a-4894-8d9c-3dd0e34a276f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "e690f632-a389-4bf4-8d20-ff20b236bc0d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ad008004-ab0a-4894-8d9c-3dd0e34a276f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f7fa36d3-66a0-4762-ace6-29af6bd6c7af",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ad008004-ab0a-4894-8d9c-3dd0e34a276f",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "ba93f3bb-b1b4-4fca-8da6-1051becc0034",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "275f06af-02ed-4237-877c-07ab1d724e18",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ba93f3bb-b1b4-4fca-8da6-1051becc0034",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8ad1ec29-2ada-4a0a-aeb3-84a499e1ec82",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ba93f3bb-b1b4-4fca-8da6-1051becc0034",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "e9506b82-400f-4c58-aac4-efa81972d727",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "fce37aad-b6a0-4abe-8490-e98345534bdd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9506b82-400f-4c58-aac4-efa81972d727",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31cd687b-89ed-4fca-b23d-d69b5b2b9781",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9506b82-400f-4c58-aac4-efa81972d727",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "b9d85f7b-4b85-48fa-b28f-24f24ccb7242",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "ada91795-38d9-42d1-bade-d7433988276a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b9d85f7b-4b85-48fa-b28f-24f24ccb7242",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7600456b-970a-4213-abc6-968d3bd1b3ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b9d85f7b-4b85-48fa-b28f-24f24ccb7242",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "4594598e-f8a8-4b3f-8c22-6336937cda83",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "164a13e0-199d-4c43-bd7b-c60ba9d0e4ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4594598e-f8a8-4b3f-8c22-6336937cda83",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e2f26ccb-a18c-4162-b6ec-03a191e57996",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4594598e-f8a8-4b3f-8c22-6336937cda83",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "5ae2091c-8349-4288-9bde-cf20979774f7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "70dce8fa-de15-4af2-aa6e-cad09756bbcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5ae2091c-8349-4288-9bde-cf20979774f7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a0c5d47a-b9fb-4a4a-b57a-90e1a464082f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5ae2091c-8349-4288-9bde-cf20979774f7",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "6d00c681-2320-4b5b-af8e-7dffa736e8c5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "445cb2e2-8482-4f4d-b5d8-562b1f9440d1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d00c681-2320-4b5b-af8e-7dffa736e8c5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "16c2ee17-52e3-4868-ad41-fa06c8c725b5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d00c681-2320-4b5b-af8e-7dffa736e8c5",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "b1c7c113-5464-4444-964f-e6b847125c18",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "5b8222f5-569b-4ec2-b166-cc4ea7452eed",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b1c7c113-5464-4444-964f-e6b847125c18",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d55b7c2-1803-4b74-9b2d-ae3db0217b4b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b1c7c113-5464-4444-964f-e6b847125c18",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "d9618e1c-f915-41d0-afa4-28665d567b22",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "2e79120c-12d8-4c4d-806b-c137d54fd632",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d9618e1c-f915-41d0-afa4-28665d567b22",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "951818ba-8281-4eb3-be97-8fccde160f07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d9618e1c-f915-41d0-afa4-28665d567b22",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "4829ec2e-78c2-4459-9eaa-5243c387fdc1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "c705b4f8-10ea-476b-ba8f-b941187e5c34",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4829ec2e-78c2-4459-9eaa-5243c387fdc1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2738b718-bc2e-47fe-bd80-b0a574d4bb9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4829ec2e-78c2-4459-9eaa-5243c387fdc1",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "a63545c9-5815-466d-b720-2318e9fe0e88",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "06fc31d4-53e7-4e51-925a-4e67e9fe7891",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a63545c9-5815-466d-b720-2318e9fe0e88",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b717cb1b-7200-4d27-9af0-8008ce8d1306",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a63545c9-5815-466d-b720-2318e9fe0e88",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "e2f443d7-3c82-4b1d-869d-494d9da495cd",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "44f4451d-6892-43b4-a295-9e6f1623fee8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e2f443d7-3c82-4b1d-869d-494d9da495cd",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bf16f4b-36dc-423e-8e75-5fc8b3a2ff89",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e2f443d7-3c82-4b1d-869d-494d9da495cd",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "0444cbca-0e23-4d84-87bd-5d88f263a26c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "30f118e8-4a7d-4272-a090-2665353ca527",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0444cbca-0e23-4d84-87bd-5d88f263a26c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "52453283-b9ac-4d28-a1f2-53e9093df17d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0444cbca-0e23-4d84-87bd-5d88f263a26c",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "308e61cd-8c34-4ddf-afba-cd9e8b6bafc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "161650b0-86d1-49f7-9697-7f68c5407a64",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "308e61cd-8c34-4ddf-afba-cd9e8b6bafc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30904447-c388-4e6b-b8e7-bf24dc4f4891",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "308e61cd-8c34-4ddf-afba-cd9e8b6bafc8",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        },
        {
            "id": "c48e6d0b-b05e-474e-8a0d-98d510a643e6",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "compositeImage": {
                "id": "09af29ad-ee15-45f3-b5f6-be9f36e21099",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c48e6d0b-b05e-474e-8a0d-98d510a643e6",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "0b68a57e-b114-4d15-babb-2f0619e0198f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c48e6d0b-b05e-474e-8a0d-98d510a643e6",
                    "LayerId": "02319efc-a62a-4757-9822-cb770d2e04a9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 176,
    "layers": [
        {
            "id": "02319efc-a62a-4757-9822-cb770d2e04a9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e374792b-519a-47e7-b677-0e835e20bc0f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 16,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 94,
    "yorig": 174
}