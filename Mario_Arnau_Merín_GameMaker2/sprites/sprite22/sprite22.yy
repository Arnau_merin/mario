{
    "id": "73393c3d-11a1-44a4-b81e-124727531a9c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite22",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 175,
    "bbox_left": 0,
    "bbox_right": 245,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "5a4eb7e7-0b19-4f46-8bfd-e7f2df7f1c1e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "73393c3d-11a1-44a4-b81e-124727531a9c",
            "compositeImage": {
                "id": "96af12a3-197c-4dcd-9c78-ae26044a0b44",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5a4eb7e7-0b19-4f46-8bfd-e7f2df7f1c1e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "407fd478-48f1-45ae-9b79-ee146c39f9ad",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5a4eb7e7-0b19-4f46-8bfd-e7f2df7f1c1e",
                    "LayerId": "f3818375-6d44-47b2-acb6-10d85f82dfa6"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 176,
    "layers": [
        {
            "id": "f3818375-6d44-47b2-acb6-10d85f82dfa6",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "73393c3d-11a1-44a4-b81e-124727531a9c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 246,
    "xorig": 0,
    "yorig": 0
}